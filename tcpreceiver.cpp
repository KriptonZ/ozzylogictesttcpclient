#include "tcpreceiver.h"
#include <QFile>

TCPReceiver::TCPReceiver(QObject *parent)
    : QObject{parent},
      m_Socket(this)
{
    m_Socket.connectToHost(QHostAddress("127.0.0.1"), 1111);
    connect(&m_Socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void TCPReceiver::onReadyRead()
{
    QFile file("received_response.txt");
    file.open(QIODevice::WriteOnly);

    QByteArray data;
    m_Socket.waitForDisconnected();
    data.append(m_Socket.readAll());

    qDebug() << "Data received!";

    file.write(data);
    file.close();
}
