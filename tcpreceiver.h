#ifndef TCPRECEIVER_H
#define TCPRECEIVER_H

#include <QObject>
#include <QTcpSocket>

class TCPReceiver : public QObject
{
    Q_OBJECT
public:
    explicit TCPReceiver(QObject *parent = nullptr);

public slots:
    void onReadyRead();

private:
    QTcpSocket m_Socket;
};

#endif // TCPRECEIVER_H
